import React from 'react'

import { Link } from 'react-router-dom'

const Header = () => {
    return (
        <header>
                <div className="container">
                    <div className="logo">
                        <Link to='/'>
                            JIMMY FERMIN
                        </Link>
                    </div>
                    <div className="menu">MENU</div>
                </div>
        </header>
    )
}

export default Header
