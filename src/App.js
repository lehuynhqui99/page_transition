import React from 'react'
import {AnimatePresence} from 'framer-motion'

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";


import Header from './components/header'
import Home from './pages/home'
import Model from './pages/model'

import './App.css';

function App() {
  return (
    <Router>
      <Header />
      <Route render={({location}) => (
        <AnimatePresence initial={false} exitBeforeEnter>
        <Switch location={(location)} key={(location.pathname)}>
          <Route exact path='/' render={() => <Home />}/>
          <Route exact path='/model/:id' render={() => <Model />}/>
        </Switch>
        </AnimatePresence>
      )}
      />

    </Router>
  );
}

export default App;
