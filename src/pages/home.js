import React from 'react'
import { Link } from 'react-router-dom'
import ProgressiveImage from 'react-progressive-image'
import img from '../imgs/compressed-image.jpg'
import Vimg from '../imgs/yasmeen.webp'
import {motion} from 'framer-motion'

const Home = () => {
    return(
        <section id="home">
                <div className="box">
                    <div className="frame">
                        <Link to='/model/yasmeen-tariq'>
                            <ProgressiveImage src={Vimg} placeholder={img}>
                                {src => <motion.img whileHover={{scale: 1.1}} src={src} alt="Yasmeen Tariq" />}
                            </ProgressiveImage>
                        </Link>
                    </div>
                    <div className="text">
                        <p>Y 1330090.0909</p>
                        <p>BKW1.55769.3939839</p>
                    </div>
                </div>
            </section>
    )
}

export default Home
