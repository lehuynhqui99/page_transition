import React from 'react'
import Vimg from '../imgs/yasmeen.webp'

const Model = () => {
    return(
        <section id='single'>
            <div className="container">
                <div className="top">
                    <div className="top-top">
                        <p>Y1330090.0909</p>
                        <p>BKW1.55769.3939839</p>
                    </div>
                    <div className="top-bottom">
                        <div className="first">
                            <span>Y</span>
                            <span>a</span>
                            <span>s</span>
                            <span>m</span>
                            <span>e</span>
                            <span>e</span>
                            <span>n</span>
                        </div>
                        <div className="last">
                            <span>T</span>
                            <span>a</span>
                            <span>r</span>
                            <span>i</span>
                            <span>p</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="bottom">
                    <div className="box">
                        <img src={Vimg} alt=""/>
                    </div>
            </div>
        </section>
    )
}

export default Model
